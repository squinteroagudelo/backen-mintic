from authApp.models.account import Account
from authApp.models.user import User
from django.contrib import admin

# Register your models here.
admin.site.register(User)
admin.site.register(Account)
